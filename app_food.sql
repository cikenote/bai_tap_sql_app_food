 CREATE TABLE users (
	user_id INT PRIMARY KEY AUTO_INCREMENT,
	full_name VARCHAR (255),
	email VARCHAR (255),
	password VARCHAR (255)
);

INSERT INTO users (full_name, email, password) VALUES
    ('John Doe', 'john.doe@example.com', 'password123'),
    ('Jane Smith', 'jane.smith@example.com', 'securepass'),
    ('Alice Johnson', 'alice.johnson@example.com', 'pass1234'),
    ('Bob Johnson', 'bob.johnson@example.com', 'bobpass'),
    ('Eva Brown', 'eva.brown@example.com', 'evapass'),
    ('David Miller', 'david.miller@example.com', 'davidpass'),
    ('Emma White', 'emma.white@example.com', 'emmapass'),
    ('Frank Davis', 'frank.davis@example.com', 'frankpass'),
    ('Grace Wilson', 'grace.wilson@example.com', 'gracepass'),
    ('Henry Jones', 'henry.jones@example.com', 'henrypass'),
    ('Isabel Lee', 'isabel.lee@example.com', 'isabelpass'),
    ('Jack Moore', 'jack.moore@example.com', 'jackpass'),
    ('Kelly Taylor', 'kelly.taylor@example.com', 'kellypass'),
    ('Leo Carter', 'leo.carter@example.com', 'leopass'),
    ('Mia Harris', 'mia.harris@example.com', 'miapass'),
    ('Noah Adams', 'noah.adams@example.com', 'noahpass'),
    ('Olivia Clark', 'olivia.clark@example.com', 'oliviapass'),
    ('Peter Wilson', 'peter.wilson@example.com', 'peterpass'),
    ('Quinn Martin', 'quinn.martin@example.com', 'quinnpass'),
    ('Rachel White', 'rachel.white@example.com', 'rachelpass');


CREATE TABLE restaurant(
	rest_id INT PRIMARY KEY AUTO_INCREMENT,
	res_name VARCHAR (255),
	image VARCHAR (255),
	descr VARCHAR (255)
);

INSERT INTO restaurant (res_name, image, descr) VALUES
    ('The Gourmet Kitchen', 'gourmet_kitchen.jpg', 'A fine dining experience with a variety of exquisite dishes.'),
    ('Pasta Paradise', 'pasta_paradise.jpg', 'Serving the best pasta dishes in town with a cozy atmosphere.'),
    ('Burger Bistro', 'burger_bistro.jpg', 'Home of delicious, juicy burgers and flavorful fries.'),
    ('Sushi Haven', 'sushi_haven.jpg', 'Experience the art of sushi making with our fresh and authentic ingredients.'),
    ('Mediterranean Delight', 'mediterranean_delight.jpg', 'Indulge in the flavors of the Mediterranean with our diverse menu.'),
    ('Coffee & Cozy', 'coffee_and_cozy.jpg', 'Relax with a cup of freshly brewed coffee in our cozy cafe.'),
    ('Vegetarian Delight', 'vegetarian_delight.jpg', 'Delicious vegetarian and vegan options for a healthy dining experience.'),
    ('Taco Fiesta', 'taco_fiesta.jpg', 'Savor the taste of authentic tacos and Mexican cuisine.'),
    ('Seafood Sensation', 'seafood_sensation.jpg', 'Enjoy a variety of seafood dishes prepared with the finest ingredients.'),
    ('Pizza Perfection', 'pizza_perfection.jpg', 'A pizza lover\'s paradise with a wide range of toppings and crusts.'),
    ('Steakhouse Supreme', 'steakhouse_supreme.jpg', 'Indulge in premium cuts of meat and a sophisticated dining atmosphere.'),
    ('Asian Fusion Flavors', 'asian_fusion_flavors.jpg', 'Explore the fusion of Asian cuisines in our eclectic menu.'),
    ('Healthy Greens', 'healthy_greens.jpg', 'Nourish your body with our selection of fresh and healthy salads.'),
    ('Dessert Delights', 'dessert_delights.jpg', 'Satisfy your sweet tooth with our delectable desserts and treats.'),
    ('Street Food Express', 'street_food_express.jpg', 'Experience the taste of global street food in a quick and casual setting.'),
    ('Craft Beer Hub', 'craft_beer_hub.jpg', 'Discover a diverse range of craft beers and enjoy a laid-back atmosphere.'),
    ('Family Feast Kitchen', 'family_feast_kitchen.jpg', 'A family-friendly restaurant offering a variety of dishes for all tastes.'),
    ('Wine & Dine Lounge', 'wine_and_dine_lounge.jpg', 'Relish the combination of fine wines and gourmet dishes in an elegant setting.'),
    ('BBQ Grill Master', 'bbq_grill_master.jpg', 'Savor the smoky flavors of expertly grilled barbecue dishes.'),
    ('Farm-to-Table Eats', 'farm_to_table_eats.jpg', 'Experience the freshness of locally sourced ingredients in our farm-to-table concept.');

CREATE TABLE food_type (
	type_id INT PRIMARY KEY AUTO_INCREMENT,
	type_name VARCHAR (255)
);

INSERT INTO food_type (type_name) VALUES
    ('Appetizer'),
    ('Soup'),
    ('Salad'),
    ('Pasta'),
    ('Burger'),
    ('Sushi'),
    ('Mediterranean'),
    ('Coffee'),
    ('Vegetarian'),
    ('Taco'),
    ('Seafood'),
    ('Pizza'),
    ('Steak'),
    ('Asian Fusion'),
    ('Healthy'),
    ('Dessert'),
    ('Street Food'),
    ('Craft Beer'),
    ('Family Feast'),
    ('Wine & Dine');

CREATE TABLE food (
	food_id INT PRIMARY KEY AUTO_INCREMENT,
	food_name VARCHAR (255),
	image VARCHAR (255),
	price FLOAT,
	descr VARCHAR (255),
	type_id INT,
	FOREIGN KEY(type_id) REFERENCES food_type(type_id)
);

INSERT INTO food (food_name, image, price, descr, type_id) VALUES
    ('Caprese Salad', 'caprese_salad.jpg', 12.99, 'Fresh tomatoes, mozzarella, and basil drizzled with balsamic glaze.', 3),
    ('Chicken Alfredo Pasta', 'chicken_alfredo_pasta.jpg', 15.99, 'Creamy Alfredo sauce with grilled chicken and fettuccine noodles.', 4),
    ('Classic Cheeseburger', 'cheeseburger.jpg', 9.99, 'Juicy beef patty with cheese, lettuce, and tomato on a sesame seed bun.', 5),
    ('Dragon Roll', 'dragon_roll.jpg', 18.99, 'Sushi roll with shrimp tempura, avocado, and eel sauce.', 6),
    ('Greek Gyro', 'greek_gyro.jpg', 11.99, 'Grilled lamb, tomatoes, onions, and tzatziki sauce wrapped in pita bread.', 7),
    ('Espresso', 'espresso.jpg', 3.99, 'Strong and concentrated coffee served in a small cup.', 8),
    ('Vegetarian Pizza', 'vegetarian_pizza.jpg', 14.99, 'Pizza topped with assorted vegetables and melted cheese.', 13),
    ('Taco Trio', 'taco_trio.jpg', 10.99, 'Assorted tacos with your choice of fillings and toppings.', 9),
    ('Grilled Salmon', 'grilled_salmon.jpg', 22.99, 'Fresh salmon fillet grilled to perfection with lemon and herbs.', 10),
    ('Margherita Pizza', 'margherita_pizza.jpg', 13.99, 'Classic pizza with tomato, mozzarella, and basil.', 13),
    ('Filet Mignon', 'filet_mignon.jpg', 29.99, 'Tender filet mignon steak cooked to your liking.', 12),
    ('Sushi Platter', 'sushi_platter.jpg', 24.99, 'Assorted sushi rolls and sashimi pieces for sharing.', 6),
    ('Mango Tango Smoothie', 'mango_tango_smoothie.jpg', 6.99, 'Refreshing smoothie with mango, banana, and yogurt.', 15),
    ('Chocolate Lava Cake', 'chocolate_lava_cake.jpg', 8.99, 'Warm chocolate cake with a gooey, molten center.', 16),
    ('Street Tacos', 'street_tacos.jpg', 11.99, 'Authentic street-style tacos with various meat options.', 9),
    ('Craft Beer Flight', 'craft_beer_flight.jpg', 18.99, 'Sampler of craft beers to explore different flavors.', 18),
    ('Family Combo Meal', 'family_combo_meal.jpg', 30.99, 'A combination of dishes suitable for a family feast.', 19),
    ('Cabernet Sauvignon', 'cabernet_sauvignon.jpg', 25.99, 'Full-bodied red wine with rich flavors of dark fruits.', 20),
    ('BBQ Chicken Wings', 'bbq_chicken_wings.jpg', 9.99, 'Crispy chicken wings glazed with barbecue sauce.', 5),
    ('Avocado Salad', 'avocado_salad.jpg', 13.99, 'Salad with fresh avocado, mixed greens, and vinaigrette dressing.', 3);

CREATE TABLE sub_food (
    sub_id INT PRIMARY KEY AUTO_INCREMENT,
    sub_name VARCHAR(255),
    sub_price FLOAT,
    food_id INT,
    FOREIGN KEY(food_id) REFERENCES food(food_id)
);

INSERT INTO sub_food (sub_name, sub_price, food_id) VALUES
    ('Extra Cheese', 1.99, 1),
    ('Garlic Bread', 3.49, 2),
    ('Caesar Salad', 4.99, 3),
    ('Meatball Sub', 5.99, 4),
    ('Tempura Roll', 6.99, 5),
    ('Hummus Platter', 4.49, 6),
    ('Cappuccino', 3.99, 7),
    ('Vegan Pizza', 12.99, 8),
    ('Guacamole', 2.99, 9),
    ('Fish Tacos', 7.99, 10),
    ('Calzone', 8.99, 11),
    ('Rainbow Roll', 14.99, 12),
    ('Berry Smoothie', 5.99, 13),
    ('Cheesecake', 7.49, 14),
    ('Nachos', 9.99, 15),
    ('IPA Beer', 6.99, 16),
    ('Family Size Combo', 25.99, 17),
    ('Chardonnay', 18.99, 18),
    ('Buffalo Wings', 8.49, 19),
    ('Mango Avocado Salad', 10.99, 20);


CREATE TABLE orders (
	user_id INT,
	food_id INT,
	amount INT,
	code VARCHAR (255),
	arr_sub_id VARCHAR (255),
	FOREIGN KEY(food_id) REFERENCES food(food_id),
	FOREIGN KEY(user_id) REFERENCES users(user_id)
);

INSERT INTO orders (user_id, food_id, amount, code, arr_sub_id) VALUES
    (1, 3, 2, 'ORDER123', '1,2'),
    (2, 7, 1, 'ORDER456', '3'),
    (3, 12, 3, 'ORDER789', '4,5,6'),
    (4, 2, 1, 'ORDERABC', '7'),
    (5, 16, 2, 'ORDERDEF', '8,9'),
    (1, 9, 1, 'ORDERGHI', '10'),
    (2, 5, 4, 'ORDERJKL', '11,12,13,14'),
    (3, 20, 1, 'ORDERMNO', '15'),
    (4, 14, 2, 'ORDERPQR', '16,17'),
    (5, 8, 3, 'ORDERSUV', '18,19,20'),
    (1, 1, 1, 'ORDERTWX', '1'),
    (2, 11, 2, 'ORDERYZA', '2,3'),
    (3, 18, 1, 'ORDERBCD', '4'),
    (4, 4, 3, 'ORDEREFG', '5,6,7'),
    (5, 13, 1, 'ORDERHIJ', '8'),
    (1, 6, 2, 'ORDERKLM', '9,10'),
    (2, 19, 3, 'ORDERNOP', '11,12,13'),
    (3, 10, 1, 'ORDERQRS', '14'),
    (4, 15, 2, 'ORDERTUV', '15,16'),
    (5, 17, 1, 'ORDERWXY', '17');

CREATE TABLE rate_res (
	user_id INT,
	rest_id INT,
	amount INT, 
	date_rate DATETIME,
	FOREIGN KEY(user_id) REFERENCES users(user_id),
	FOREIGN KEY(rest_id) REFERENCES restaurant(rest_id)
);

INSERT INTO rate_res (user_id, rest_id, amount, date_rate) VALUES
    (1, 1, 4, '2024-01-01 08:30:00'),
    (2, 2, 5, '2024-02-10 12:45:00'),
    (3, 3, 3, '2024-03-15 18:15:00'),
    (4, 4, 4, '2024-04-20 20:00:00'),
    (5, 5, 5, '2024-05-25 14:30:00'),
    (1, 6, 4, '2024-06-05 10:20:00'),
    (2, 7, 5, '2024-07-12 16:45:00'),
    (3, 8, 3, '2024-08-18 19:30:00'),
    (4, 9, 4, '2024-09-24 21:15:00'),
    (5, 10, 5, '2024-10-01 13:40:00'),
    (1, 11, 4, '2024-11-07 11:10:00'),
    (2, 12, 5, '2024-12-14 17:25:00'),
    (3, 13, 3, '2025-01-20 22:00:00'),
    (4, 14, 4, '2025-02-25 08:50:00'),
    (5, 15, 5, '2025-03-02 15:30:00'),
    (1, 16, 4, '2025-04-08 09:40:00'),
    (2, 17, 5, '2025-05-15 14:55:00'),
    (3, 18, 3, '2025-06-22 20:10:00'),
    (4, 19, 4, '2025-07-29 23:45:00'),
    (5, 20, 5, '2025-08-05 12:05:00');

CREATE TABLE like_res(
	user_id INT,
	rest_id INT,
	date_like DATETIME,
	FOREIGN KEY(user_id) REFERENCES users(user_id),
	FOREIGN KEY(rest_id) REFERENCES restaurant(rest_id)
);

INSERT INTO like_res (user_id, rest_id, date_like)
VALUES
    (1, 5, '2024-01-01 21:11:00'),
    (2, 3, '2024-01-01 21:12:00'),
    (3, 1, '2024-01-01 21:13:00'),
    (4, 2, '2024-01-01 21:14:00'),
    (5, 4, '2024-01-01 21:15:00'),
    (6, 6, '2024-01-01 21:16:00'),
    (7, 7, '2024-01-01 21:17:00'),
    (8, 8, '2024-01-01 21:18:00'),
    (9, 9, '2024-01-01 21:19:00'),
    (10, 10, '2024-01-01 21:20:00'),
    (11, 11, '2024-01-01 21:21:00'),
    (12, 12, '2024-01-01 21:22:00'),
    (13, 13, '2024-01-01 21:23:00'),
    (14, 14, '2024-01-01 21:24:00'),
    (15, 15, '2024-01-01 21:25:00'),
    (16, 16, '2024-01-01 21:26:00'),
    (17, 17, '2024-01-01 21:27:00'),
    (18, 18, '2024-01-01 21:28:00'),
    (19, 19, '2024-01-01 21:29:00'),
    (20, 20, '2024-01-01 21:30:00');


-- Tìm 5 người đã like nhà hàng nhiều nhất
SELECT user_id, COUNT(*) as like_count
FROM like_res
GROUP BY user_id
ORDER BY like_count DESC
LIMIT 5;


-- Tìm 2 nhà hàng có lượt like nhiều nhất
SELECT rest_id, COUNT(*) as like_count
FROM like_res
GROUP BY rest_id
ORDER BY like_count DESC
LIMIT 2;

-- Tìm người đã đặt hàng nhiều nhất
SELECT user_id, COUNT(*) as order_count
FROM orders
GROUP BY user_id
ORDER BY order_count DESC
LIMIT 1;

-- Tìm người dùng không hoạt động trong hệ thống (không đặt hàng, không like, không đánh giá nhà hàng)
SELECT users.user_id
FROM users
LEFT JOIN orders ON users.user_id = orders.user_id
LEFT JOIN like_res ON users.user_id = like_res.user_id
LEFT JOIN rate_res ON users.user_id = rate_res.user_id
WHERE orders.user_id IS NULL AND like_res.user_id IS NULL AND rate_res.user_id IS NULL;


